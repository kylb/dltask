## How to Install

1. `git clone https://gitlab.com/kylb/dltask` 
2. Entre na pasta do projeto via terminal e execute os comandos abaixo
1. `cp .env.example .env`
1. `php artisan key:generate`
1. `php composer install`
1. `npm install` (Pode pular por enquanto)
1. `php artisan migrate:fresh --seed` (Pode pular por enquanto)
1. `php artisan serve`
1. Acesse: `http://localhost:8000/`

 ```sh
 echo 'be happy!'
 
 ```